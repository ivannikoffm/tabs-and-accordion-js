let tabHeadList = document.querySelectorAll('.tab');
let accordionElements = document.querySelectorAll('.acc-tab');

for (let i = 0; i < tabHeadList.length; i++) {
    tabHeadList[i].addEventListener('click', tabOnClick.bind(this, i, tabHeadList[i]));
}

for (let i = 0; i < accordionElements.length; i++) {
    accordionElements[i].addEventListener('click', accordionOnClick);
}

function tabOnClick(index, currentTarget) {
    document.querySelector('.activetab').classList.toggle('activetab');
    currentTarget.classList.toggle('activetab');

    let contentBlocks = document.querySelectorAll('.tab-content');

    document.querySelector('.activetabcontent').classList.toggle('activetabcontent');
    contentBlocks[index].classList.toggle('activetabcontent');
};

function accordionOnClick(event) {
    let ct = event.currentTarget;
    // In case current accord-tab is enabled and its clicked
    if (ct.classList.contains('activeaccordtab')) {
        
        ct.classList.toggle('activeaccordtab');
        ct.nextSibling.style.height = '0px';
        return;
    }
    // In case another tab is enabled
    else if (document.querySelector('.activeaccordtab')) {
        let target = document.querySelector('.activeaccordtab');
        target.classList.toggle('activeaccordtab');       
        
        target.nextSibling.classList.toggle('accordion-active');
        target.nextSibling.style.height = '0px';
    }

    ct.classList.toggle('activeaccordtab');
    ct.nextSibling.classList.toggle('accordion-active');
    ct.nextSibling.style.height = ct.nextSibling.scrollHeight + 'px';
}
